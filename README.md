## Usage

#### Backup Server Setup
1. Clone the repo:
```
git clone https://gitlab.com/carverhaines/elkarbackup-docker
```

2.  Copy and edit .env file with your version of elkarbackup, db password, timezone, backup directory and SMTP server if used:
```
cp .env.example .env
vim .env
```

3. Run the stack:
```
docker-compose up -d
```

Login with root:root at localhost:8000, change password.

#### Backup Client Setup
1. On clients (any server you want to backup), install rsync:
```
apt update && apt install rsync   # ubuntu/debian systems
apk update && apk add rsync       # alpine
yum update && yum install rsync   # centos/redhat/fedora
```
2. Set servers to allow root ssh access via keyfile:
`vim /etc/ssh/sshd_config`
```
PermitRootLogin prohibit-password
```

3. Then use curl to get the backup server's public key and add it to root's authorized_keys file, using your server's IP:
```
mkdir -p /root/.ssh
curl -k http://X.X.X.X:3001/config/publickey/get >> /root/.ssh/authorized_keys
```
